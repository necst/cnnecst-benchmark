/*
 * Copyright 2016 Andrea Solazzo, Emanuele Del Sozzo, Gianluca Durelli, Matteo De Silvestri, Irene De Rose
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * 	http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include "cnn.h"

//HW Libraries
#include "supportLib.h"
#include <fcntl.h>

#define REAL float
#define SIZE 1000

#define INPUT_SIZE FM_0*DIMH_0*DIMW_0

/*Results tensor*/
#define CLASSES 10

int main ()
{
	REAL x,y,v,max = -HUGE_VAL;
	int labels[SIZE];
	double input_double[SIZE][INPUT_SIZE];
	float input[SIZE][INPUT_SIZE];
	int i,j,k,l,index;
	unsigned predCPU[SIZE], predFPGA[SIZE];
	unsigned countCPU = 0, countFPGA = 0;
	REAL output;
	REAL total;
	REAL error;
	int retval;

	FILE *tf;
	unsigned long starttimeFPGA, endtimeFPGA, starttimeCPU,endtimeCPU, timeCPU, timeFPGA;
	
	//Importing input images and labels
	FILE *img;
	char name_prefix[] = "data_bin/test";
	char *name = (char *) malloc (sizeof (name_prefix) + 4);


	int axiDmaFd = open("/dev/axi-dma1", O_RDWR);
    if(axiDmaFd < 0){
        printf("Error in opening file\n");
        exit(0);
    }
    std::cout << "DMA file open" << std::endl;


	printf ("\nImporting images.....");

	for (i = 0; i < SIZE; i++){
		sprintf (name, "%s%d", name_prefix, i);
		img = fopen (name, "rb");

		if (!img){
			fprintf (stderr, "\n\tError while opening file %s!\n", name);
			exit (1);
		}
		
		for(l = 0; l < FM_0; l++){
			for (j = 0; j < DIMH_0; j++){
				for (k = 0; k < DIMW_0; k++){
					fread (&input_double[i][l*DIMH_0*DIMW_0 + j*DIMW_0 + k], sizeof (double), 1, img);	//Reads image bytes
				}
			}
		}
		
		fread (&labels[i], sizeof (int), 1, img);					//Reads label
		fclose (img);
	}

	for(i = 0; i < SIZE; i++){
		for(int j = 0; j < INPUT_SIZE; j++){
			input[i][j] = (float)input_double[i][j];
		}
	}

	printf ("Done!\n");
	
	
	/* SOFTWARE EXECUTION */

	std::cout << "Start CPU execution" << std::endl;

    starttimeCPU = getTime();


	for (index = 0; index < SIZE; index++){	
		predCPU[index] = cnn(input[index]) + 1;
	}

 	endtimeCPU = getTime();

 	std::cout << "End CPU execution" << std::endl;
    
    timeCPU = endtimeCPU - starttimeCPU;


    FILE *fp = fopen("resultsCPU.dat", "w");    
    fprintf(fp, "Index\tClass\n");

	for (index = 0; index < SIZE; index++){	
		fprintf(fp, "%d\t%d\n", index, predCPU[index]-1);
		
		if (predCPU[index] != labels[index])	
			countCPU++;
	}
	
	fclose(fp);
	
	total = (REAL) SIZE;
	error = (REAL) countCPU;
	error = error / total;

	printf ("\n\nCPU Test error %.3f ( %u mistakes out of %u samples )\n", error, countCPU, SIZE);
	
	std::cout << "Execution Time CPU:\t" << timeCPU << std::endl;
	//
	// end CPU execution
	//



	//
	// FPGA execution
	//

	std::cout << "Start FPGA execution" << std::endl;
	starttimeFPGA = getTime();	

	for (index = 0; index < SIZE; index++){

		writeDMA(axiDmaFd, (unsigned char *) input[index], INPUT_SIZE*sizeof(REAL));
		readDMA(axiDmaFd, (unsigned char *) &output, sizeof(REAL));
		predFPGA[index] = output + 1;
    }

    endtimeFPGA = getTime();
    
    std::cout << "End FPGA execution" << std::endl;
    
    timeFPGA = endtimeFPGA - starttimeFPGA;


    fp = fopen("resultsFPGA.dat", "w");
    
    fprintf(fp, "Index\tClass\n");
    for (index = 0; index < SIZE; index++){
		fprintf(fp, "%d\t%d\n", index, predFPGA[index]-1);
		
		if (predFPGA[index] != labels[index])
			countFPGA++;
	}
    
	fclose(fp);    
    
	total = (REAL) SIZE;
	error = (REAL) countFPGA;
	error = error / total;

	printf ("\n\nFPGA Test error %.3f ( %u mistakes out of %u samples )\n", error, countFPGA, SIZE);

	//
	// end FPGA execution
	//
	
	std::cout << "Execution Time HW:\t" << timeFPGA << std::endl;
	
	return 0;
}
