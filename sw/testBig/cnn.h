/*
 * Copyright 2016 Andrea Solazzo, Emanuele Del Sozzo, Gianluca Durelli, Matteo De Silvestri, Irene De Rose
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * 	http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#ifndef __CNN_H__
#define __CNN_H__
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define REAL float

#define FM_0 1
#define DIMW_0 16
#define DIMH_0 16
int cnn(REAL input[FM_0*DIMH_0*DIMW_0]);

#endif
