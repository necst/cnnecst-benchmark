// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module cnn_Loop_Lin1_proc (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_continue,
        ap_idle,
        ap_ready,
        lin_address0,
        lin_ce0,
        lin_q0,
        l1_address0,
        l1_ce0,
        l1_we0,
        l1_d0,
        l1_q0,
        ap_return
);

parameter    ap_const_logic_1 = 1'b1;
parameter    ap_const_logic_0 = 1'b0;
parameter    ap_ST_st1_fsm_0 = 49'b1;
parameter    ap_ST_st2_fsm_1 = 49'b10;
parameter    ap_ST_st3_fsm_2 = 49'b100;
parameter    ap_ST_st4_fsm_3 = 49'b1000;
parameter    ap_ST_st5_fsm_4 = 49'b10000;
parameter    ap_ST_st6_fsm_5 = 49'b100000;
parameter    ap_ST_st7_fsm_6 = 49'b1000000;
parameter    ap_ST_st8_fsm_7 = 49'b10000000;
parameter    ap_ST_st9_fsm_8 = 49'b100000000;
parameter    ap_ST_st10_fsm_9 = 49'b1000000000;
parameter    ap_ST_st11_fsm_10 = 49'b10000000000;
parameter    ap_ST_st12_fsm_11 = 49'b100000000000;
parameter    ap_ST_st13_fsm_12 = 49'b1000000000000;
parameter    ap_ST_st14_fsm_13 = 49'b10000000000000;
parameter    ap_ST_st15_fsm_14 = 49'b100000000000000;
parameter    ap_ST_st16_fsm_15 = 49'b1000000000000000;
parameter    ap_ST_st17_fsm_16 = 49'b10000000000000000;
parameter    ap_ST_st18_fsm_17 = 49'b100000000000000000;
parameter    ap_ST_st19_fsm_18 = 49'b1000000000000000000;
parameter    ap_ST_st20_fsm_19 = 49'b10000000000000000000;
parameter    ap_ST_st21_fsm_20 = 49'b100000000000000000000;
parameter    ap_ST_st22_fsm_21 = 49'b1000000000000000000000;
parameter    ap_ST_st23_fsm_22 = 49'b10000000000000000000000;
parameter    ap_ST_st24_fsm_23 = 49'b100000000000000000000000;
parameter    ap_ST_st25_fsm_24 = 49'b1000000000000000000000000;
parameter    ap_ST_st26_fsm_25 = 49'b10000000000000000000000000;
parameter    ap_ST_st27_fsm_26 = 49'b100000000000000000000000000;
parameter    ap_ST_st28_fsm_27 = 49'b1000000000000000000000000000;
parameter    ap_ST_st29_fsm_28 = 49'b10000000000000000000000000000;
parameter    ap_ST_st30_fsm_29 = 49'b100000000000000000000000000000;
parameter    ap_ST_st31_fsm_30 = 49'b1000000000000000000000000000000;
parameter    ap_ST_st32_fsm_31 = 49'b10000000000000000000000000000000;
parameter    ap_ST_st33_fsm_32 = 49'b100000000000000000000000000000000;
parameter    ap_ST_st34_fsm_33 = 49'b1000000000000000000000000000000000;
parameter    ap_ST_st35_fsm_34 = 49'b10000000000000000000000000000000000;
parameter    ap_ST_st36_fsm_35 = 49'b100000000000000000000000000000000000;
parameter    ap_ST_st37_fsm_36 = 49'b1000000000000000000000000000000000000;
parameter    ap_ST_st38_fsm_37 = 49'b10000000000000000000000000000000000000;
parameter    ap_ST_st39_fsm_38 = 49'b100000000000000000000000000000000000000;
parameter    ap_ST_st40_fsm_39 = 49'b1000000000000000000000000000000000000000;
parameter    ap_ST_st41_fsm_40 = 49'b10000000000000000000000000000000000000000;
parameter    ap_ST_st42_fsm_41 = 49'b100000000000000000000000000000000000000000;
parameter    ap_ST_st43_fsm_42 = 49'b1000000000000000000000000000000000000000000;
parameter    ap_ST_st44_fsm_43 = 49'b10000000000000000000000000000000000000000000;
parameter    ap_ST_st45_fsm_44 = 49'b100000000000000000000000000000000000000000000;
parameter    ap_ST_st46_fsm_45 = 49'b1000000000000000000000000000000000000000000000;
parameter    ap_ST_st47_fsm_46 = 49'b10000000000000000000000000000000000000000000000;
parameter    ap_ST_st48_fsm_47 = 49'b100000000000000000000000000000000000000000000000;
parameter    ap_ST_st49_fsm_48 = 49'b1000000000000000000000000000000000000000000000000;
parameter    ap_const_lv32_0 = 32'b00000000000000000000000000000000;
parameter    ap_const_lv1_1 = 1'b1;
parameter    ap_const_lv32_C = 32'b1100;
parameter    ap_const_lv32_17 = 32'b10111;
parameter    ap_const_lv32_E = 32'b1110;
parameter    ap_const_lv32_12 = 32'b10010;
parameter    ap_const_lv32_1 = 32'b1;
parameter    ap_const_lv1_0 = 1'b0;
parameter    ap_const_lv32_2 = 32'b10;
parameter    ap_const_lv32_3 = 32'b11;
parameter    ap_const_lv32_7 = 32'b111;
parameter    ap_const_lv32_D = 32'b1101;
parameter    ap_const_lv32_F = 32'b1111;
parameter    ap_const_lv32_10 = 32'b10000;
parameter    ap_const_lv32_11 = 32'b10001;
parameter    ap_const_lv32_18 = 32'b11000;
parameter    ap_const_lv32_2A = 32'b101010;
parameter    ap_const_lv32_2B = 32'b101011;
parameter    ap_const_lv32_30 = 32'b110000;
parameter    ap_const_lv4_0 = 4'b0000;
parameter    ap_const_lv12_0 = 12'b000000000000;
parameter    ap_const_lv8_0 = 8'b00000000;
parameter    ap_const_lv32_FF800000 = 32'b11111111100000000000000000000000;
parameter    ap_const_lv32_2C = 32'b101100;
parameter    ap_const_lv32_8 = 32'b1000;
parameter    ap_const_lv32_13 = 32'b10011;
parameter    ap_const_lv12_D8 = 12'b11011000;
parameter    ap_const_lv4_A = 4'b1010;
parameter    ap_const_lv4_1 = 4'b1;
parameter    ap_const_lv8_D8 = 8'b11011000;
parameter    ap_const_lv8_1 = 8'b1;
parameter    ap_const_lv32_1E = 32'b11110;
parameter    ap_const_lv8_FF = 8'b11111111;
parameter    ap_const_lv23_0 = 23'b00000000000000000000000;
parameter    ap_const_lv2_0 = 2'b00;
parameter    ap_const_lv2_1 = 2'b1;
parameter    ap_const_lv5_2 = 5'b10;
parameter    ap_const_lv64_0 = 64'b0000000000000000000000000000000000000000000000000000000000000000;
parameter    ap_true = 1'b1;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
input   ap_continue;
output   ap_idle;
output   ap_ready;
output  [7:0] lin_address0;
output   lin_ce0;
input  [31:0] lin_q0;
output  [3:0] l1_address0;
output   l1_ce0;
output   l1_we0;
output  [31:0] l1_d0;
input  [31:0] l1_q0;
output  [31:0] ap_return;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg lin_ce0;
reg[3:0] l1_address0;
reg l1_ce0;
reg l1_we0;
reg[31:0] l1_d0;
reg    ap_done_reg = 1'b0;
(* fsm_encoding = "none" *) reg   [48:0] ap_CS_fsm = 49'b1;
reg    ap_sig_cseq_ST_st1_fsm_0;
reg    ap_sig_bdd_68;
wire   [3:0] lb1_address0;
reg    lb1_ce0;
wire   [31:0] lb1_q0;
wire   [11:0] lw1_address0;
reg    lw1_ce0;
wire   [31:0] lw1_q0;
wire   [31:0] grp_fu_214_p2;
reg   [31:0] reg_243;
reg    ap_sig_cseq_ST_st13_fsm_12;
reg    ap_sig_bdd_111;
reg    ap_sig_cseq_ST_st24_fsm_23;
reg    ap_sig_bdd_118;
reg   [31:0] reg_249;
reg    ap_sig_cseq_ST_st15_fsm_14;
reg    ap_sig_bdd_127;
reg    ap_sig_cseq_ST_st19_fsm_18;
reg    ap_sig_bdd_134;
wire   [11:0] next_mul_fu_255_p2;
reg   [11:0] next_mul_reg_436;
reg    ap_sig_cseq_ST_st2_fsm_1;
reg    ap_sig_bdd_144;
wire   [3:0] i_1_fu_267_p2;
reg   [3:0] i_1_reg_444;
wire   [0:0] exitcond5_i_fu_261_p2;
reg   [3:0] l1_addr_reg_454;
reg    ap_sig_cseq_ST_st3_fsm_2;
reg    ap_sig_bdd_162;
wire   [7:0] j_fu_285_p2;
reg   [7:0] j_reg_467;
reg    ap_sig_cseq_ST_st4_fsm_3;
reg    ap_sig_bdd_171;
wire   [0:0] exitcond4_i_fu_279_p2;
wire   [31:0] grp_fu_221_p2;
reg   [31:0] v_reg_492;
reg    ap_sig_cseq_ST_st8_fsm_7;
reg    ap_sig_bdd_196;
wire   [3:0] i_fu_317_p2;
reg   [3:0] i_reg_500;
reg    ap_sig_cseq_ST_st14_fsm_13;
reg    ap_sig_bdd_205;
wire   [0:0] exitcond3_i_fu_311_p2;
wire   [0:0] tmp_12_fu_233_p2;
reg   [0:0] tmp_12_reg_510;
reg    ap_sig_cseq_ST_st16_fsm_15;
reg    ap_sig_bdd_219;
wire   [31:0] max_2_fu_411_p3;
reg    ap_sig_cseq_ST_st17_fsm_16;
reg    ap_sig_bdd_228;
wire   [3:0] i_2_fu_425_p2;
reg   [3:0] i_2_reg_523;
reg    ap_sig_cseq_ST_st18_fsm_17;
reg    ap_sig_bdd_237;
reg   [3:0] l1_addr_2_reg_528;
wire   [0:0] exitcond2_i_fu_419_p2;
wire   [63:0] tmp_33_i_fu_230_p1;
reg   [63:0] tmp_33_i_reg_533;
reg    ap_sig_cseq_ST_st25_fsm_24;
reg    ap_sig_bdd_251;
wire   [63:0] grp_fu_238_p2;
reg   [63:0] tmp_34_i_reg_538;
reg    ap_sig_cseq_ST_st43_fsm_42;
reg    ap_sig_bdd_260;
wire   [31:0] tmp_35_i_fu_227_p1;
reg   [31:0] tmp_35_i_reg_543;
reg    ap_sig_cseq_ST_st44_fsm_43;
reg    ap_sig_bdd_269;
reg    ap_sig_cseq_ST_st49_fsm_48;
reg    ap_sig_bdd_277;
reg   [3:0] i_6_i_reg_123;
reg    ap_sig_bdd_287;
reg   [11:0] phi_mul_reg_134;
reg   [31:0] storemerge_reg_146;
reg   [7:0] j_5_i_reg_157;
reg   [31:0] max_3_i_reg_168;
reg   [3:0] i_7_i_reg_180;
reg   [31:0] x_out_reg_191;
reg   [3:0] i_8_i_reg_203;
wire   [63:0] tmp_14_i_fu_273_p1;
wire   [63:0] tmp_34_cast_fu_306_p1;
wire   [63:0] tmp_24_i_fu_291_p1;
wire   [63:0] tmp_22_i_fu_323_p1;
wire   [63:0] tmp_32_i_fu_431_p1;
reg    ap_sig_cseq_ST_st45_fsm_44;
reg    ap_sig_bdd_322;
reg   [31:0] grp_fu_214_p0;
reg   [31:0] grp_fu_214_p1;
reg    ap_sig_cseq_ST_st9_fsm_8;
reg    ap_sig_bdd_334;
reg    ap_sig_cseq_ST_st20_fsm_19;
reg    ap_sig_bdd_341;
wire   [11:0] tmp_24_i_cast_fu_296_p1;
wire   [11:0] tmp_s_fu_300_p2;
wire   [31:0] max_to_int_fu_328_p1;
wire   [31:0] max_3_i_to_int_fu_346_p1;
wire   [7:0] tmp_fu_332_p4;
wire   [22:0] tmp_22_fu_342_p1;
wire   [0:0] notrhs_fu_370_p2;
wire   [0:0] notlhs_fu_364_p2;
wire   [7:0] tmp_7_fu_350_p4;
wire   [22:0] tmp_23_fu_360_p1;
wire   [0:0] notrhs2_fu_388_p2;
wire   [0:0] notlhs1_fu_382_p2;
wire   [0:0] tmp_9_fu_376_p2;
wire   [0:0] tmp_10_fu_394_p2;
wire   [0:0] tmp_11_fu_400_p2;
wire   [0:0] tmp_13_fu_406_p2;
reg   [1:0] grp_fu_214_opcode;
wire    grp_fu_214_ce;
wire    grp_fu_221_ce;
wire   [4:0] tmp_12_fu_233_opcode;
wire    grp_fu_238_ce;
reg   [48:0] ap_NS_fsm;


cnn_Loop_Lin1_proc_lb1 #(
    .DataWidth( 32 ),
    .AddressRange( 10 ),
    .AddressWidth( 4 ))
lb1_U(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .address0( lb1_address0 ),
    .ce0( lb1_ce0 ),
    .q0( lb1_q0 )
);

cnn_Loop_Lin1_proc_lw1 #(
    .DataWidth( 32 ),
    .AddressRange( 2160 ),
    .AddressWidth( 12 ))
lw1_U(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .address0( lw1_address0 ),
    .ce0( lw1_ce0 ),
    .q0( lw1_q0 )
);

cnn_faddfsub_32ns_32ns_32_5_full_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 5 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
cnn_faddfsub_32ns_32ns_32_5_full_dsp_U27(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( grp_fu_214_p0 ),
    .din1( grp_fu_214_p1 ),
    .opcode( grp_fu_214_opcode ),
    .ce( grp_fu_214_ce ),
    .dout( grp_fu_214_p2 )
);

cnn_fmul_32ns_32ns_32_4_max_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 4 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
cnn_fmul_32ns_32ns_32_4_max_dsp_U28(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( lin_q0 ),
    .din1( lw1_q0 ),
    .ce( grp_fu_221_ce ),
    .dout( grp_fu_221_p2 )
);

cnn_fptrunc_64ns_32_1 #(
    .ID( 1 ),
    .NUM_STAGE( 1 ),
    .din0_WIDTH( 64 ),
    .dout_WIDTH( 32 ))
cnn_fptrunc_64ns_32_1_U29(
    .din0( tmp_34_i_reg_538 ),
    .dout( tmp_35_i_fu_227_p1 )
);

cnn_fpext_32ns_64_1 #(
    .ID( 1 ),
    .NUM_STAGE( 1 ),
    .din0_WIDTH( 32 ),
    .dout_WIDTH( 64 ))
cnn_fpext_32ns_64_1_U30(
    .din0( reg_243 ),
    .dout( tmp_33_i_fu_230_p1 )
);

cnn_fcmp_32ns_32ns_1_1 #(
    .ID( 1 ),
    .NUM_STAGE( 1 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 1 ))
cnn_fcmp_32ns_32ns_1_1_U31(
    .din0( reg_249 ),
    .din1( max_3_i_reg_168 ),
    .opcode( tmp_12_fu_233_opcode ),
    .dout( tmp_12_fu_233_p2 )
);

cnn_dexp_64ns_64ns_64_18_full_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 18 ),
    .din0_WIDTH( 64 ),
    .din1_WIDTH( 64 ),
    .dout_WIDTH( 64 ))
cnn_dexp_64ns_64ns_64_18_full_dsp_U32(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( ap_const_lv64_0 ),
    .din1( tmp_33_i_reg_533 ),
    .ce( grp_fu_238_ce ),
    .dout( grp_fu_238_p2 )
);



always @ (posedge ap_clk) begin : ap_ret_ap_CS_fsm
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_st1_fsm_0;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin : ap_ret_ap_done_reg
    if (ap_rst == 1'b1) begin
        ap_done_reg <= ap_const_logic_0;
    end else begin
        if ((ap_const_logic_1 == ap_continue)) begin
            ap_done_reg <= ap_const_logic_0;
        end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17) & ~(ap_const_lv1_0 == exitcond2_i_fu_419_p2))) begin
            ap_done_reg <= ap_const_logic_1;
        end
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~ap_sig_bdd_287)) begin
        i_6_i_reg_123 <= ap_const_lv4_0;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3) & ~(ap_const_lv1_0 == exitcond4_i_fu_279_p2))) begin
        i_6_i_reg_123 <= i_1_reg_444;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & ~(exitcond5_i_fu_261_p2 == ap_const_lv1_0))) begin
        i_7_i_reg_180 <= ap_const_lv4_0;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st17_fsm_16)) begin
        i_7_i_reg_180 <= i_reg_500;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st14_fsm_13) & ~(ap_const_lv1_0 == exitcond3_i_fu_311_p2))) begin
        i_8_i_reg_203 <= ap_const_lv4_0;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st49_fsm_48)) begin
        i_8_i_reg_203 <= i_2_reg_523;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st13_fsm_12)) begin
        j_5_i_reg_157 <= j_reg_467;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        j_5_i_reg_157 <= ap_const_lv8_0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & ~(exitcond5_i_fu_261_p2 == ap_const_lv1_0))) begin
        max_3_i_reg_168 <= ap_const_lv32_FF800000;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st17_fsm_16)) begin
        max_3_i_reg_168 <= max_2_fu_411_p3;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~ap_sig_bdd_287)) begin
        phi_mul_reg_134 <= ap_const_lv12_0;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3) & ~(ap_const_lv1_0 == exitcond4_i_fu_279_p2))) begin
        phi_mul_reg_134 <= next_mul_reg_436;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st13_fsm_12)) begin
        storemerge_reg_146 <= grp_fu_214_p2;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        storemerge_reg_146 <= lb1_q0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st14_fsm_13) & ~(ap_const_lv1_0 == exitcond3_i_fu_311_p2))) begin
        x_out_reg_191 <= ap_const_lv32_0;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st49_fsm_48)) begin
        x_out_reg_191 <= grp_fu_214_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1)) begin
        i_1_reg_444 <= i_1_fu_267_p2;
        next_mul_reg_436 <= next_mul_fu_255_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17)) begin
        i_2_reg_523 <= i_2_fu_425_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st14_fsm_13)) begin
        i_reg_500 <= i_fu_317_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3)) begin
        j_reg_467 <= j_fu_285_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17) & (ap_const_lv1_0 == exitcond2_i_fu_419_p2))) begin
        l1_addr_2_reg_528 <= tmp_32_i_fu_431_p1;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & (exitcond5_i_fu_261_p2 == ap_const_lv1_0))) begin
        l1_addr_reg_454 <= tmp_14_i_fu_273_p1;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st13_fsm_12) | (ap_const_logic_1 == ap_sig_cseq_ST_st24_fsm_23))) begin
        reg_243 <= grp_fu_214_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st15_fsm_14) | (ap_const_logic_1 == ap_sig_cseq_ST_st19_fsm_18))) begin
        reg_249 <= l1_q0;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st16_fsm_15)) begin
        tmp_12_reg_510 <= tmp_12_fu_233_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st25_fsm_24)) begin
        tmp_33_i_reg_533 <= tmp_33_i_fu_230_p1;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st43_fsm_42)) begin
        tmp_34_i_reg_538 <= grp_fu_238_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st44_fsm_43)) begin
        tmp_35_i_reg_543 <= tmp_35_i_fu_227_p1;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7)) begin
        v_reg_492 <= grp_fu_221_p2;
    end
end

always @ (ap_done_reg or ap_sig_cseq_ST_st18_fsm_17 or exitcond2_i_fu_419_p2) begin
    if (((ap_const_logic_1 == ap_done_reg) | ((ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17) & ~(ap_const_lv1_0 == exitcond2_i_fu_419_p2)))) begin
        ap_done = ap_const_logic_1;
    end else begin
        ap_done = ap_const_logic_0;
    end
end

always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0) begin
    if ((~(ap_const_logic_1 == ap_start) & (ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0))) begin
        ap_idle = ap_const_logic_1;
    end else begin
        ap_idle = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st18_fsm_17 or exitcond2_i_fu_419_p2) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17) & ~(ap_const_lv1_0 == exitcond2_i_fu_419_p2))) begin
        ap_ready = ap_const_logic_1;
    end else begin
        ap_ready = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_111) begin
    if (ap_sig_bdd_111) begin
        ap_sig_cseq_ST_st13_fsm_12 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st13_fsm_12 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_205) begin
    if (ap_sig_bdd_205) begin
        ap_sig_cseq_ST_st14_fsm_13 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st14_fsm_13 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_127) begin
    if (ap_sig_bdd_127) begin
        ap_sig_cseq_ST_st15_fsm_14 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st15_fsm_14 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_219) begin
    if (ap_sig_bdd_219) begin
        ap_sig_cseq_ST_st16_fsm_15 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st16_fsm_15 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_228) begin
    if (ap_sig_bdd_228) begin
        ap_sig_cseq_ST_st17_fsm_16 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st17_fsm_16 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_237) begin
    if (ap_sig_bdd_237) begin
        ap_sig_cseq_ST_st18_fsm_17 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st18_fsm_17 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_134) begin
    if (ap_sig_bdd_134) begin
        ap_sig_cseq_ST_st19_fsm_18 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st19_fsm_18 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_68) begin
    if (ap_sig_bdd_68) begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_341) begin
    if (ap_sig_bdd_341) begin
        ap_sig_cseq_ST_st20_fsm_19 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st20_fsm_19 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_118) begin
    if (ap_sig_bdd_118) begin
        ap_sig_cseq_ST_st24_fsm_23 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st24_fsm_23 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_251) begin
    if (ap_sig_bdd_251) begin
        ap_sig_cseq_ST_st25_fsm_24 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st25_fsm_24 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_144) begin
    if (ap_sig_bdd_144) begin
        ap_sig_cseq_ST_st2_fsm_1 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st2_fsm_1 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_162) begin
    if (ap_sig_bdd_162) begin
        ap_sig_cseq_ST_st3_fsm_2 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st3_fsm_2 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_260) begin
    if (ap_sig_bdd_260) begin
        ap_sig_cseq_ST_st43_fsm_42 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st43_fsm_42 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_269) begin
    if (ap_sig_bdd_269) begin
        ap_sig_cseq_ST_st44_fsm_43 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st44_fsm_43 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_322) begin
    if (ap_sig_bdd_322) begin
        ap_sig_cseq_ST_st45_fsm_44 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st45_fsm_44 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_277) begin
    if (ap_sig_bdd_277) begin
        ap_sig_cseq_ST_st49_fsm_48 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st49_fsm_48 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_171) begin
    if (ap_sig_bdd_171) begin
        ap_sig_cseq_ST_st4_fsm_3 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st4_fsm_3 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_196) begin
    if (ap_sig_bdd_196) begin
        ap_sig_cseq_ST_st8_fsm_7 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st8_fsm_7 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_334) begin
    if (ap_sig_bdd_334) begin
        ap_sig_cseq_ST_st9_fsm_8 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st9_fsm_8 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st45_fsm_44 or ap_sig_cseq_ST_st9_fsm_8 or ap_sig_cseq_ST_st20_fsm_19) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st20_fsm_19)) begin
        grp_fu_214_opcode = ap_const_lv2_1;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st45_fsm_44) | (ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8))) begin
        grp_fu_214_opcode = ap_const_lv2_0;
    end else begin
        grp_fu_214_opcode = 'bx;
    end
end

always @ (reg_249 or storemerge_reg_146 or x_out_reg_191 or ap_sig_cseq_ST_st45_fsm_44 or ap_sig_cseq_ST_st9_fsm_8 or ap_sig_cseq_ST_st20_fsm_19) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st45_fsm_44)) begin
        grp_fu_214_p0 = x_out_reg_191;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st20_fsm_19)) begin
        grp_fu_214_p0 = reg_249;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8)) begin
        grp_fu_214_p0 = storemerge_reg_146;
    end else begin
        grp_fu_214_p0 = 'bx;
    end
end

always @ (v_reg_492 or tmp_35_i_reg_543 or max_3_i_reg_168 or ap_sig_cseq_ST_st45_fsm_44 or ap_sig_cseq_ST_st9_fsm_8 or ap_sig_cseq_ST_st20_fsm_19) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st45_fsm_44)) begin
        grp_fu_214_p1 = tmp_35_i_reg_543;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st20_fsm_19)) begin
        grp_fu_214_p1 = max_3_i_reg_168;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8)) begin
        grp_fu_214_p1 = v_reg_492;
    end else begin
        grp_fu_214_p1 = 'bx;
    end
end

always @ (l1_addr_reg_454 or ap_sig_cseq_ST_st4_fsm_3 or ap_sig_cseq_ST_st14_fsm_13 or ap_sig_cseq_ST_st18_fsm_17 or l1_addr_2_reg_528 or tmp_22_i_fu_323_p1 or tmp_32_i_fu_431_p1 or ap_sig_cseq_ST_st45_fsm_44) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st45_fsm_44)) begin
        l1_address0 = l1_addr_2_reg_528;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3)) begin
        l1_address0 = l1_addr_reg_454;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17)) begin
        l1_address0 = tmp_32_i_fu_431_p1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st14_fsm_13)) begin
        l1_address0 = tmp_22_i_fu_323_p1;
    end else begin
        l1_address0 = 'bx;
    end
end

always @ (ap_sig_cseq_ST_st4_fsm_3 or ap_sig_cseq_ST_st14_fsm_13 or ap_sig_cseq_ST_st18_fsm_17 or ap_sig_cseq_ST_st45_fsm_44) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3) | (ap_const_logic_1 == ap_sig_cseq_ST_st14_fsm_13) | (ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17) | (ap_const_logic_1 == ap_sig_cseq_ST_st45_fsm_44))) begin
        l1_ce0 = ap_const_logic_1;
    end else begin
        l1_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st4_fsm_3 or tmp_35_i_reg_543 or storemerge_reg_146 or ap_sig_cseq_ST_st45_fsm_44) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st45_fsm_44)) begin
        l1_d0 = tmp_35_i_reg_543;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3)) begin
        l1_d0 = storemerge_reg_146;
    end else begin
        l1_d0 = 'bx;
    end
end

always @ (ap_sig_cseq_ST_st4_fsm_3 or ap_sig_cseq_ST_st45_fsm_44) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3) | (ap_const_logic_1 == ap_sig_cseq_ST_st45_fsm_44))) begin
        l1_we0 = ap_const_logic_1;
    end else begin
        l1_we0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st2_fsm_1) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1)) begin
        lb1_ce0 = ap_const_logic_1;
    end else begin
        lb1_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st4_fsm_3) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3)) begin
        lin_ce0 = ap_const_logic_1;
    end else begin
        lin_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st4_fsm_3) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3)) begin
        lw1_ce0 = ap_const_logic_1;
    end else begin
        lw1_ce0 = ap_const_logic_0;
    end
end
always @ (ap_CS_fsm or exitcond5_i_fu_261_p2 or exitcond4_i_fu_279_p2 or exitcond3_i_fu_311_p2 or exitcond2_i_fu_419_p2 or ap_sig_bdd_287) begin
    case (ap_CS_fsm)
        ap_ST_st1_fsm_0 : 
        begin
            if (~ap_sig_bdd_287) begin
                ap_NS_fsm = ap_ST_st2_fsm_1;
            end else begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end
        end
        ap_ST_st2_fsm_1 : 
        begin
            if (~(exitcond5_i_fu_261_p2 == ap_const_lv1_0)) begin
                ap_NS_fsm = ap_ST_st14_fsm_13;
            end else begin
                ap_NS_fsm = ap_ST_st3_fsm_2;
            end
        end
        ap_ST_st3_fsm_2 : 
        begin
            ap_NS_fsm = ap_ST_st4_fsm_3;
        end
        ap_ST_st4_fsm_3 : 
        begin
            if (~(ap_const_lv1_0 == exitcond4_i_fu_279_p2)) begin
                ap_NS_fsm = ap_ST_st2_fsm_1;
            end else begin
                ap_NS_fsm = ap_ST_st5_fsm_4;
            end
        end
        ap_ST_st5_fsm_4 : 
        begin
            ap_NS_fsm = ap_ST_st6_fsm_5;
        end
        ap_ST_st6_fsm_5 : 
        begin
            ap_NS_fsm = ap_ST_st7_fsm_6;
        end
        ap_ST_st7_fsm_6 : 
        begin
            ap_NS_fsm = ap_ST_st8_fsm_7;
        end
        ap_ST_st8_fsm_7 : 
        begin
            ap_NS_fsm = ap_ST_st9_fsm_8;
        end
        ap_ST_st9_fsm_8 : 
        begin
            ap_NS_fsm = ap_ST_st10_fsm_9;
        end
        ap_ST_st10_fsm_9 : 
        begin
            ap_NS_fsm = ap_ST_st11_fsm_10;
        end
        ap_ST_st11_fsm_10 : 
        begin
            ap_NS_fsm = ap_ST_st12_fsm_11;
        end
        ap_ST_st12_fsm_11 : 
        begin
            ap_NS_fsm = ap_ST_st13_fsm_12;
        end
        ap_ST_st13_fsm_12 : 
        begin
            ap_NS_fsm = ap_ST_st4_fsm_3;
        end
        ap_ST_st14_fsm_13 : 
        begin
            if (~(ap_const_lv1_0 == exitcond3_i_fu_311_p2)) begin
                ap_NS_fsm = ap_ST_st18_fsm_17;
            end else begin
                ap_NS_fsm = ap_ST_st15_fsm_14;
            end
        end
        ap_ST_st15_fsm_14 : 
        begin
            ap_NS_fsm = ap_ST_st16_fsm_15;
        end
        ap_ST_st16_fsm_15 : 
        begin
            ap_NS_fsm = ap_ST_st17_fsm_16;
        end
        ap_ST_st17_fsm_16 : 
        begin
            ap_NS_fsm = ap_ST_st14_fsm_13;
        end
        ap_ST_st18_fsm_17 : 
        begin
            if (~(ap_const_lv1_0 == exitcond2_i_fu_419_p2)) begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end else begin
                ap_NS_fsm = ap_ST_st19_fsm_18;
            end
        end
        ap_ST_st19_fsm_18 : 
        begin
            ap_NS_fsm = ap_ST_st20_fsm_19;
        end
        ap_ST_st20_fsm_19 : 
        begin
            ap_NS_fsm = ap_ST_st21_fsm_20;
        end
        ap_ST_st21_fsm_20 : 
        begin
            ap_NS_fsm = ap_ST_st22_fsm_21;
        end
        ap_ST_st22_fsm_21 : 
        begin
            ap_NS_fsm = ap_ST_st23_fsm_22;
        end
        ap_ST_st23_fsm_22 : 
        begin
            ap_NS_fsm = ap_ST_st24_fsm_23;
        end
        ap_ST_st24_fsm_23 : 
        begin
            ap_NS_fsm = ap_ST_st25_fsm_24;
        end
        ap_ST_st25_fsm_24 : 
        begin
            ap_NS_fsm = ap_ST_st26_fsm_25;
        end
        ap_ST_st26_fsm_25 : 
        begin
            ap_NS_fsm = ap_ST_st27_fsm_26;
        end
        ap_ST_st27_fsm_26 : 
        begin
            ap_NS_fsm = ap_ST_st28_fsm_27;
        end
        ap_ST_st28_fsm_27 : 
        begin
            ap_NS_fsm = ap_ST_st29_fsm_28;
        end
        ap_ST_st29_fsm_28 : 
        begin
            ap_NS_fsm = ap_ST_st30_fsm_29;
        end
        ap_ST_st30_fsm_29 : 
        begin
            ap_NS_fsm = ap_ST_st31_fsm_30;
        end
        ap_ST_st31_fsm_30 : 
        begin
            ap_NS_fsm = ap_ST_st32_fsm_31;
        end
        ap_ST_st32_fsm_31 : 
        begin
            ap_NS_fsm = ap_ST_st33_fsm_32;
        end
        ap_ST_st33_fsm_32 : 
        begin
            ap_NS_fsm = ap_ST_st34_fsm_33;
        end
        ap_ST_st34_fsm_33 : 
        begin
            ap_NS_fsm = ap_ST_st35_fsm_34;
        end
        ap_ST_st35_fsm_34 : 
        begin
            ap_NS_fsm = ap_ST_st36_fsm_35;
        end
        ap_ST_st36_fsm_35 : 
        begin
            ap_NS_fsm = ap_ST_st37_fsm_36;
        end
        ap_ST_st37_fsm_36 : 
        begin
            ap_NS_fsm = ap_ST_st38_fsm_37;
        end
        ap_ST_st38_fsm_37 : 
        begin
            ap_NS_fsm = ap_ST_st39_fsm_38;
        end
        ap_ST_st39_fsm_38 : 
        begin
            ap_NS_fsm = ap_ST_st40_fsm_39;
        end
        ap_ST_st40_fsm_39 : 
        begin
            ap_NS_fsm = ap_ST_st41_fsm_40;
        end
        ap_ST_st41_fsm_40 : 
        begin
            ap_NS_fsm = ap_ST_st42_fsm_41;
        end
        ap_ST_st42_fsm_41 : 
        begin
            ap_NS_fsm = ap_ST_st43_fsm_42;
        end
        ap_ST_st43_fsm_42 : 
        begin
            ap_NS_fsm = ap_ST_st44_fsm_43;
        end
        ap_ST_st44_fsm_43 : 
        begin
            ap_NS_fsm = ap_ST_st45_fsm_44;
        end
        ap_ST_st45_fsm_44 : 
        begin
            ap_NS_fsm = ap_ST_st46_fsm_45;
        end
        ap_ST_st46_fsm_45 : 
        begin
            ap_NS_fsm = ap_ST_st47_fsm_46;
        end
        ap_ST_st47_fsm_46 : 
        begin
            ap_NS_fsm = ap_ST_st48_fsm_47;
        end
        ap_ST_st48_fsm_47 : 
        begin
            ap_NS_fsm = ap_ST_st49_fsm_48;
        end
        ap_ST_st49_fsm_48 : 
        begin
            ap_NS_fsm = ap_ST_st18_fsm_17;
        end
        default : 
        begin
            ap_NS_fsm = 'bx;
        end
    endcase
end


assign ap_return = x_out_reg_191;


always @ (ap_CS_fsm) begin
    ap_sig_bdd_111 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_C]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_118 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_17]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_127 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_E]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_134 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_12]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_144 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_1]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_162 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_2]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_171 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_3]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_196 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_7]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_205 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_D]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_219 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_F]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_228 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_10]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_237 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_11]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_251 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_18]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_260 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_2A]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_269 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_2B]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_277 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_30]);
end


always @ (ap_start or ap_done_reg) begin
    ap_sig_bdd_287 = ((ap_start == ap_const_logic_0) | (ap_done_reg == ap_const_logic_1));
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_322 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_2C]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_334 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_8]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_341 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_13]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_68 = (ap_CS_fsm[ap_const_lv32_0] == ap_const_lv1_1);
end

assign exitcond2_i_fu_419_p2 = (i_8_i_reg_203 == ap_const_lv4_A? 1'b1: 1'b0);

assign exitcond3_i_fu_311_p2 = (i_7_i_reg_180 == ap_const_lv4_A? 1'b1: 1'b0);

assign exitcond4_i_fu_279_p2 = (j_5_i_reg_157 == ap_const_lv8_D8? 1'b1: 1'b0);

assign exitcond5_i_fu_261_p2 = (i_6_i_reg_123 == ap_const_lv4_A? 1'b1: 1'b0);

assign grp_fu_214_ce = ap_const_logic_1;

assign grp_fu_221_ce = ap_const_logic_1;

assign grp_fu_238_ce = ap_const_logic_1;

assign i_1_fu_267_p2 = (i_6_i_reg_123 + ap_const_lv4_1);

assign i_2_fu_425_p2 = (i_8_i_reg_203 + ap_const_lv4_1);

assign i_fu_317_p2 = (i_7_i_reg_180 + ap_const_lv4_1);

assign j_fu_285_p2 = (j_5_i_reg_157 + ap_const_lv8_1);

assign lb1_address0 = tmp_14_i_fu_273_p1;

assign lin_address0 = tmp_24_i_fu_291_p1;

assign lw1_address0 = tmp_34_cast_fu_306_p1;

assign max_2_fu_411_p3 = ((tmp_13_fu_406_p2[0:0] === 1'b1) ? reg_249 : max_3_i_reg_168);

assign max_3_i_to_int_fu_346_p1 = max_3_i_reg_168;

assign max_to_int_fu_328_p1 = reg_249;

assign next_mul_fu_255_p2 = (phi_mul_reg_134 + ap_const_lv12_D8);

assign notlhs1_fu_382_p2 = (tmp_7_fu_350_p4 != ap_const_lv8_FF? 1'b1: 1'b0);

assign notlhs_fu_364_p2 = (tmp_fu_332_p4 != ap_const_lv8_FF? 1'b1: 1'b0);

assign notrhs2_fu_388_p2 = (tmp_23_fu_360_p1 == ap_const_lv23_0? 1'b1: 1'b0);

assign notrhs_fu_370_p2 = (tmp_22_fu_342_p1 == ap_const_lv23_0? 1'b1: 1'b0);

assign tmp_10_fu_394_p2 = (notrhs2_fu_388_p2 | notlhs1_fu_382_p2);

assign tmp_11_fu_400_p2 = (tmp_9_fu_376_p2 & tmp_10_fu_394_p2);

assign tmp_12_fu_233_opcode = ap_const_lv5_2;

assign tmp_13_fu_406_p2 = (tmp_11_fu_400_p2 & tmp_12_reg_510);

assign tmp_14_i_fu_273_p1 = i_6_i_reg_123;

assign tmp_22_fu_342_p1 = max_to_int_fu_328_p1[22:0];

assign tmp_22_i_fu_323_p1 = i_7_i_reg_180;

assign tmp_23_fu_360_p1 = max_3_i_to_int_fu_346_p1[22:0];

assign tmp_24_i_cast_fu_296_p1 = j_5_i_reg_157;

assign tmp_24_i_fu_291_p1 = j_5_i_reg_157;

assign tmp_32_i_fu_431_p1 = i_8_i_reg_203;

assign tmp_34_cast_fu_306_p1 = tmp_s_fu_300_p2;

assign tmp_7_fu_350_p4 = {{max_3_i_to_int_fu_346_p1[ap_const_lv32_1E : ap_const_lv32_17]}};

assign tmp_9_fu_376_p2 = (notrhs_fu_370_p2 | notlhs_fu_364_p2);

assign tmp_fu_332_p4 = {{max_to_int_fu_328_p1[ap_const_lv32_1E : ap_const_lv32_17]}};

assign tmp_s_fu_300_p2 = (phi_mul_reg_134 + tmp_24_i_cast_fu_296_p1);


endmodule //cnn_Loop_Lin1_proc

