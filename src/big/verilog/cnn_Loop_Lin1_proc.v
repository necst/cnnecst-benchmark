// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module cnn_Loop_Lin1_proc (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_continue,
        ap_idle,
        ap_ready,
        lin_address0,
        lin_ce0,
        lin_q0,
        l1_address0,
        l1_ce0,
        l1_we0,
        l1_d0,
        l1_q0,
        ap_return
);

parameter    ap_const_logic_1 = 1'b1;
parameter    ap_const_logic_0 = 1'b0;
parameter    ap_ST_st1_fsm_0 = 49'b1;
parameter    ap_ST_st2_fsm_1 = 49'b10;
parameter    ap_ST_st3_fsm_2 = 49'b100;
parameter    ap_ST_st4_fsm_3 = 49'b1000;
parameter    ap_ST_st5_fsm_4 = 49'b10000;
parameter    ap_ST_st6_fsm_5 = 49'b100000;
parameter    ap_ST_st7_fsm_6 = 49'b1000000;
parameter    ap_ST_st8_fsm_7 = 49'b10000000;
parameter    ap_ST_st9_fsm_8 = 49'b100000000;
parameter    ap_ST_st10_fsm_9 = 49'b1000000000;
parameter    ap_ST_st11_fsm_10 = 49'b10000000000;
parameter    ap_ST_st12_fsm_11 = 49'b100000000000;
parameter    ap_ST_st13_fsm_12 = 49'b1000000000000;
parameter    ap_ST_st14_fsm_13 = 49'b10000000000000;
parameter    ap_ST_st15_fsm_14 = 49'b100000000000000;
parameter    ap_ST_st16_fsm_15 = 49'b1000000000000000;
parameter    ap_ST_st17_fsm_16 = 49'b10000000000000000;
parameter    ap_ST_st18_fsm_17 = 49'b100000000000000000;
parameter    ap_ST_st19_fsm_18 = 49'b1000000000000000000;
parameter    ap_ST_st20_fsm_19 = 49'b10000000000000000000;
parameter    ap_ST_st21_fsm_20 = 49'b100000000000000000000;
parameter    ap_ST_st22_fsm_21 = 49'b1000000000000000000000;
parameter    ap_ST_st23_fsm_22 = 49'b10000000000000000000000;
parameter    ap_ST_st24_fsm_23 = 49'b100000000000000000000000;
parameter    ap_ST_st25_fsm_24 = 49'b1000000000000000000000000;
parameter    ap_ST_st26_fsm_25 = 49'b10000000000000000000000000;
parameter    ap_ST_st27_fsm_26 = 49'b100000000000000000000000000;
parameter    ap_ST_st28_fsm_27 = 49'b1000000000000000000000000000;
parameter    ap_ST_st29_fsm_28 = 49'b10000000000000000000000000000;
parameter    ap_ST_st30_fsm_29 = 49'b100000000000000000000000000000;
parameter    ap_ST_st31_fsm_30 = 49'b1000000000000000000000000000000;
parameter    ap_ST_st32_fsm_31 = 49'b10000000000000000000000000000000;
parameter    ap_ST_st33_fsm_32 = 49'b100000000000000000000000000000000;
parameter    ap_ST_st34_fsm_33 = 49'b1000000000000000000000000000000000;
parameter    ap_ST_st35_fsm_34 = 49'b10000000000000000000000000000000000;
parameter    ap_ST_st36_fsm_35 = 49'b100000000000000000000000000000000000;
parameter    ap_ST_st37_fsm_36 = 49'b1000000000000000000000000000000000000;
parameter    ap_ST_st38_fsm_37 = 49'b10000000000000000000000000000000000000;
parameter    ap_ST_st39_fsm_38 = 49'b100000000000000000000000000000000000000;
parameter    ap_ST_st40_fsm_39 = 49'b1000000000000000000000000000000000000000;
parameter    ap_ST_st41_fsm_40 = 49'b10000000000000000000000000000000000000000;
parameter    ap_ST_st42_fsm_41 = 49'b100000000000000000000000000000000000000000;
parameter    ap_ST_st43_fsm_42 = 49'b1000000000000000000000000000000000000000000;
parameter    ap_ST_st44_fsm_43 = 49'b10000000000000000000000000000000000000000000;
parameter    ap_ST_st45_fsm_44 = 49'b100000000000000000000000000000000000000000000;
parameter    ap_ST_st46_fsm_45 = 49'b1000000000000000000000000000000000000000000000;
parameter    ap_ST_st47_fsm_46 = 49'b10000000000000000000000000000000000000000000000;
parameter    ap_ST_st48_fsm_47 = 49'b100000000000000000000000000000000000000000000000;
parameter    ap_ST_st49_fsm_48 = 49'b1000000000000000000000000000000000000000000000000;
parameter    ap_const_lv32_0 = 32'b00000000000000000000000000000000;
parameter    ap_const_lv1_1 = 1'b1;
parameter    ap_const_lv32_C = 32'b1100;
parameter    ap_const_lv32_17 = 32'b10111;
parameter    ap_const_lv32_E = 32'b1110;
parameter    ap_const_lv32_12 = 32'b10010;
parameter    ap_const_lv32_1 = 32'b1;
parameter    ap_const_lv1_0 = 1'b0;
parameter    ap_const_lv32_2 = 32'b10;
parameter    ap_const_lv32_3 = 32'b11;
parameter    ap_const_lv32_7 = 32'b111;
parameter    ap_const_lv32_D = 32'b1101;
parameter    ap_const_lv32_F = 32'b1111;
parameter    ap_const_lv32_10 = 32'b10000;
parameter    ap_const_lv32_11 = 32'b10001;
parameter    ap_const_lv32_18 = 32'b11000;
parameter    ap_const_lv32_2A = 32'b101010;
parameter    ap_const_lv32_2B = 32'b101011;
parameter    ap_const_lv32_30 = 32'b110000;
parameter    ap_const_lv4_0 = 4'b0000;
parameter    ap_const_lv7_0 = 7'b0000000;
parameter    ap_const_lv32_FF800000 = 32'b11111111100000000000000000000000;
parameter    ap_const_lv32_2C = 32'b101100;
parameter    ap_const_lv32_8 = 32'b1000;
parameter    ap_const_lv32_13 = 32'b10011;
parameter    ap_const_lv4_A = 4'b1010;
parameter    ap_const_lv4_1 = 4'b1;
parameter    ap_const_lv6_0 = 6'b000000;
parameter    ap_const_lv7_40 = 7'b1000000;
parameter    ap_const_lv7_1 = 7'b1;
parameter    ap_const_lv32_1E = 32'b11110;
parameter    ap_const_lv8_FF = 8'b11111111;
parameter    ap_const_lv23_0 = 23'b00000000000000000000000;
parameter    ap_const_lv2_0 = 2'b00;
parameter    ap_const_lv2_1 = 2'b1;
parameter    ap_const_lv5_2 = 5'b10;
parameter    ap_const_lv64_0 = 64'b0000000000000000000000000000000000000000000000000000000000000000;
parameter    ap_true = 1'b1;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
input   ap_continue;
output   ap_idle;
output   ap_ready;
output  [5:0] lin_address0;
output   lin_ce0;
input  [31:0] lin_q0;
output  [3:0] l1_address0;
output   l1_ce0;
output   l1_we0;
output  [31:0] l1_d0;
input  [31:0] l1_q0;
output  [31:0] ap_return;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg lin_ce0;
reg[3:0] l1_address0;
reg l1_ce0;
reg l1_we0;
reg[31:0] l1_d0;
reg    ap_done_reg = 1'b0;
(* fsm_encoding = "none" *) reg   [48:0] ap_CS_fsm = 49'b1;
reg    ap_sig_cseq_ST_st1_fsm_0;
reg    ap_sig_bdd_68;
wire   [3:0] lb1_address0;
reg    lb1_ce0;
wire   [31:0] lb1_q0;
wire   [9:0] lw1_address0;
reg    lw1_ce0;
wire   [31:0] lw1_q0;
wire   [31:0] grp_fu_203_p2;
reg   [31:0] reg_232;
reg    ap_sig_cseq_ST_st13_fsm_12;
reg    ap_sig_bdd_111;
reg    ap_sig_cseq_ST_st24_fsm_23;
reg    ap_sig_bdd_118;
reg   [31:0] reg_238;
reg    ap_sig_cseq_ST_st15_fsm_14;
reg    ap_sig_bdd_127;
reg    ap_sig_cseq_ST_st19_fsm_18;
reg    ap_sig_bdd_134;
wire   [3:0] i_1_fu_250_p2;
reg   [3:0] i_1_reg_433;
reg    ap_sig_cseq_ST_st2_fsm_1;
reg    ap_sig_bdd_144;
wire   [0:0] exitcond5_i_fu_244_p2;
reg   [3:0] l1_addr_reg_443;
wire   [10:0] tmp_33_cast_fu_270_p1;
reg   [10:0] tmp_33_cast_reg_448;
reg    ap_sig_cseq_ST_st3_fsm_2;
reg    ap_sig_bdd_161;
wire   [6:0] j_fu_280_p2;
reg   [6:0] j_reg_461;
reg    ap_sig_cseq_ST_st4_fsm_3;
reg    ap_sig_bdd_171;
wire   [0:0] exitcond4_i_fu_274_p2;
wire   [31:0] grp_fu_210_p2;
reg   [31:0] v_reg_486;
reg    ap_sig_cseq_ST_st8_fsm_7;
reg    ap_sig_bdd_196;
wire   [3:0] i_fu_311_p2;
reg   [3:0] i_reg_494;
reg    ap_sig_cseq_ST_st14_fsm_13;
reg    ap_sig_bdd_205;
wire   [0:0] exitcond3_i_fu_305_p2;
wire   [0:0] tmp_12_fu_222_p2;
reg   [0:0] tmp_12_reg_504;
reg    ap_sig_cseq_ST_st16_fsm_15;
reg    ap_sig_bdd_219;
wire   [31:0] max_2_fu_405_p3;
reg    ap_sig_cseq_ST_st17_fsm_16;
reg    ap_sig_bdd_228;
wire   [3:0] i_2_fu_419_p2;
reg   [3:0] i_2_reg_517;
reg    ap_sig_cseq_ST_st18_fsm_17;
reg    ap_sig_bdd_237;
reg   [3:0] l1_addr_2_reg_522;
wire   [0:0] exitcond2_i_fu_413_p2;
wire   [63:0] tmp_45_i_fu_219_p1;
reg   [63:0] tmp_45_i_reg_527;
reg    ap_sig_cseq_ST_st25_fsm_24;
reg    ap_sig_bdd_251;
wire   [63:0] grp_fu_227_p2;
reg   [63:0] tmp_46_i_reg_532;
reg    ap_sig_cseq_ST_st43_fsm_42;
reg    ap_sig_bdd_260;
wire   [31:0] tmp_47_i_fu_216_p1;
reg   [31:0] tmp_47_i_reg_537;
reg    ap_sig_cseq_ST_st44_fsm_43;
reg    ap_sig_bdd_269;
reg    ap_sig_cseq_ST_st49_fsm_48;
reg    ap_sig_bdd_277;
reg   [3:0] i_8_i_reg_123;
reg    ap_sig_bdd_287;
reg   [31:0] storemerge_reg_135;
reg   [6:0] j_7_i_reg_146;
reg   [31:0] max_3_i_reg_157;
reg   [3:0] i_9_i_reg_169;
reg   [31:0] x_out_reg_180;
reg   [3:0] i_i_reg_192;
wire   [63:0] tmp_28_i_fu_256_p1;
wire   [63:0] tmp_34_cast_fu_300_p1;
wire   [63:0] tmp_38_i_fu_286_p1;
wire   [63:0] tmp_36_i_fu_317_p1;
wire   [63:0] tmp_44_i_fu_425_p1;
reg    ap_sig_cseq_ST_st45_fsm_44;
reg    ap_sig_bdd_319;
reg   [31:0] grp_fu_203_p0;
reg   [31:0] grp_fu_203_p1;
reg    ap_sig_cseq_ST_st9_fsm_8;
reg    ap_sig_bdd_331;
reg    ap_sig_cseq_ST_st20_fsm_19;
reg    ap_sig_bdd_338;
wire   [9:0] tmp_s_fu_262_p3;
wire   [10:0] tmp_38_i_cast_fu_291_p1;
wire   [10:0] tmp_20_fu_295_p2;
wire   [31:0] max_to_int_fu_322_p1;
wire   [31:0] max_3_i_to_int_fu_340_p1;
wire   [7:0] tmp_fu_326_p4;
wire   [22:0] tmp_22_fu_336_p1;
wire   [0:0] notrhs_fu_364_p2;
wire   [0:0] notlhs_fu_358_p2;
wire   [7:0] tmp_7_fu_344_p4;
wire   [22:0] tmp_23_fu_354_p1;
wire   [0:0] notrhs2_fu_382_p2;
wire   [0:0] notlhs1_fu_376_p2;
wire   [0:0] tmp_9_fu_370_p2;
wire   [0:0] tmp_10_fu_388_p2;
wire   [0:0] tmp_11_fu_394_p2;
wire   [0:0] tmp_13_fu_400_p2;
reg   [1:0] grp_fu_203_opcode;
wire    grp_fu_203_ce;
wire    grp_fu_210_ce;
wire   [4:0] tmp_12_fu_222_opcode;
wire    grp_fu_227_ce;
reg   [48:0] ap_NS_fsm;


cnn_Loop_Lin1_proc_lb1 #(
    .DataWidth( 32 ),
    .AddressRange( 10 ),
    .AddressWidth( 4 ))
lb1_U(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .address0( lb1_address0 ),
    .ce0( lb1_ce0 ),
    .q0( lb1_q0 )
);

cnn_Loop_Lin1_proc_lw1 #(
    .DataWidth( 32 ),
    .AddressRange( 640 ),
    .AddressWidth( 10 ))
lw1_U(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .address0( lw1_address0 ),
    .ce0( lw1_ce0 ),
    .q0( lw1_q0 )
);

cnn_faddfsub_32ns_32ns_32_5_full_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 5 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
cnn_faddfsub_32ns_32ns_32_5_full_dsp_U33(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( grp_fu_203_p0 ),
    .din1( grp_fu_203_p1 ),
    .opcode( grp_fu_203_opcode ),
    .ce( grp_fu_203_ce ),
    .dout( grp_fu_203_p2 )
);

cnn_fmul_32ns_32ns_32_4_max_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 4 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
cnn_fmul_32ns_32ns_32_4_max_dsp_U34(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( lin_q0 ),
    .din1( lw1_q0 ),
    .ce( grp_fu_210_ce ),
    .dout( grp_fu_210_p2 )
);

cnn_fptrunc_64ns_32_1 #(
    .ID( 1 ),
    .NUM_STAGE( 1 ),
    .din0_WIDTH( 64 ),
    .dout_WIDTH( 32 ))
cnn_fptrunc_64ns_32_1_U35(
    .din0( tmp_46_i_reg_532 ),
    .dout( tmp_47_i_fu_216_p1 )
);

cnn_fpext_32ns_64_1 #(
    .ID( 1 ),
    .NUM_STAGE( 1 ),
    .din0_WIDTH( 32 ),
    .dout_WIDTH( 64 ))
cnn_fpext_32ns_64_1_U36(
    .din0( reg_232 ),
    .dout( tmp_45_i_fu_219_p1 )
);

cnn_fcmp_32ns_32ns_1_1 #(
    .ID( 1 ),
    .NUM_STAGE( 1 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 1 ))
cnn_fcmp_32ns_32ns_1_1_U37(
    .din0( reg_238 ),
    .din1( max_3_i_reg_157 ),
    .opcode( tmp_12_fu_222_opcode ),
    .dout( tmp_12_fu_222_p2 )
);

cnn_dexp_64ns_64ns_64_18_full_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 18 ),
    .din0_WIDTH( 64 ),
    .din1_WIDTH( 64 ),
    .dout_WIDTH( 64 ))
cnn_dexp_64ns_64ns_64_18_full_dsp_U38(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( ap_const_lv64_0 ),
    .din1( tmp_45_i_reg_527 ),
    .ce( grp_fu_227_ce ),
    .dout( grp_fu_227_p2 )
);



always @ (posedge ap_clk) begin : ap_ret_ap_CS_fsm
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_st1_fsm_0;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin : ap_ret_ap_done_reg
    if (ap_rst == 1'b1) begin
        ap_done_reg <= ap_const_logic_0;
    end else begin
        if ((ap_const_logic_1 == ap_continue)) begin
            ap_done_reg <= ap_const_logic_0;
        end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17) & ~(ap_const_lv1_0 == exitcond2_i_fu_413_p2))) begin
            ap_done_reg <= ap_const_logic_1;
        end
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~ap_sig_bdd_287)) begin
        i_8_i_reg_123 <= ap_const_lv4_0;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3) & ~(ap_const_lv1_0 == exitcond4_i_fu_274_p2))) begin
        i_8_i_reg_123 <= i_1_reg_433;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & ~(exitcond5_i_fu_244_p2 == ap_const_lv1_0))) begin
        i_9_i_reg_169 <= ap_const_lv4_0;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st17_fsm_16)) begin
        i_9_i_reg_169 <= i_reg_494;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st14_fsm_13) & ~(ap_const_lv1_0 == exitcond3_i_fu_305_p2))) begin
        i_i_reg_192 <= ap_const_lv4_0;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st49_fsm_48)) begin
        i_i_reg_192 <= i_2_reg_517;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st13_fsm_12)) begin
        j_7_i_reg_146 <= j_reg_461;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        j_7_i_reg_146 <= ap_const_lv7_0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & ~(exitcond5_i_fu_244_p2 == ap_const_lv1_0))) begin
        max_3_i_reg_157 <= ap_const_lv32_FF800000;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st17_fsm_16)) begin
        max_3_i_reg_157 <= max_2_fu_405_p3;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st13_fsm_12)) begin
        storemerge_reg_135 <= grp_fu_203_p2;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        storemerge_reg_135 <= lb1_q0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st14_fsm_13) & ~(ap_const_lv1_0 == exitcond3_i_fu_305_p2))) begin
        x_out_reg_180 <= ap_const_lv32_0;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st49_fsm_48)) begin
        x_out_reg_180 <= grp_fu_203_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1)) begin
        i_1_reg_433 <= i_1_fu_250_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17)) begin
        i_2_reg_517 <= i_2_fu_419_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st14_fsm_13)) begin
        i_reg_494 <= i_fu_311_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3)) begin
        j_reg_461 <= j_fu_280_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17) & (ap_const_lv1_0 == exitcond2_i_fu_413_p2))) begin
        l1_addr_2_reg_522 <= tmp_44_i_fu_425_p1;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & (exitcond5_i_fu_244_p2 == ap_const_lv1_0))) begin
        l1_addr_reg_443 <= tmp_28_i_fu_256_p1;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st13_fsm_12) | (ap_const_logic_1 == ap_sig_cseq_ST_st24_fsm_23))) begin
        reg_232 <= grp_fu_203_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st15_fsm_14) | (ap_const_logic_1 == ap_sig_cseq_ST_st19_fsm_18))) begin
        reg_238 <= l1_q0;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st16_fsm_15)) begin
        tmp_12_reg_504 <= tmp_12_fu_222_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        tmp_33_cast_reg_448[9 : 6] <= tmp_33_cast_fu_270_p1[9 : 6];
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st25_fsm_24)) begin
        tmp_45_i_reg_527 <= tmp_45_i_fu_219_p1;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st43_fsm_42)) begin
        tmp_46_i_reg_532 <= grp_fu_227_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st44_fsm_43)) begin
        tmp_47_i_reg_537 <= tmp_47_i_fu_216_p1;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7)) begin
        v_reg_486 <= grp_fu_210_p2;
    end
end

always @ (ap_done_reg or ap_sig_cseq_ST_st18_fsm_17 or exitcond2_i_fu_413_p2) begin
    if (((ap_const_logic_1 == ap_done_reg) | ((ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17) & ~(ap_const_lv1_0 == exitcond2_i_fu_413_p2)))) begin
        ap_done = ap_const_logic_1;
    end else begin
        ap_done = ap_const_logic_0;
    end
end

always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0) begin
    if ((~(ap_const_logic_1 == ap_start) & (ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0))) begin
        ap_idle = ap_const_logic_1;
    end else begin
        ap_idle = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st18_fsm_17 or exitcond2_i_fu_413_p2) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17) & ~(ap_const_lv1_0 == exitcond2_i_fu_413_p2))) begin
        ap_ready = ap_const_logic_1;
    end else begin
        ap_ready = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_111) begin
    if (ap_sig_bdd_111) begin
        ap_sig_cseq_ST_st13_fsm_12 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st13_fsm_12 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_205) begin
    if (ap_sig_bdd_205) begin
        ap_sig_cseq_ST_st14_fsm_13 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st14_fsm_13 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_127) begin
    if (ap_sig_bdd_127) begin
        ap_sig_cseq_ST_st15_fsm_14 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st15_fsm_14 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_219) begin
    if (ap_sig_bdd_219) begin
        ap_sig_cseq_ST_st16_fsm_15 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st16_fsm_15 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_228) begin
    if (ap_sig_bdd_228) begin
        ap_sig_cseq_ST_st17_fsm_16 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st17_fsm_16 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_237) begin
    if (ap_sig_bdd_237) begin
        ap_sig_cseq_ST_st18_fsm_17 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st18_fsm_17 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_134) begin
    if (ap_sig_bdd_134) begin
        ap_sig_cseq_ST_st19_fsm_18 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st19_fsm_18 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_68) begin
    if (ap_sig_bdd_68) begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_338) begin
    if (ap_sig_bdd_338) begin
        ap_sig_cseq_ST_st20_fsm_19 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st20_fsm_19 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_118) begin
    if (ap_sig_bdd_118) begin
        ap_sig_cseq_ST_st24_fsm_23 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st24_fsm_23 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_251) begin
    if (ap_sig_bdd_251) begin
        ap_sig_cseq_ST_st25_fsm_24 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st25_fsm_24 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_144) begin
    if (ap_sig_bdd_144) begin
        ap_sig_cseq_ST_st2_fsm_1 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st2_fsm_1 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_161) begin
    if (ap_sig_bdd_161) begin
        ap_sig_cseq_ST_st3_fsm_2 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st3_fsm_2 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_260) begin
    if (ap_sig_bdd_260) begin
        ap_sig_cseq_ST_st43_fsm_42 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st43_fsm_42 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_269) begin
    if (ap_sig_bdd_269) begin
        ap_sig_cseq_ST_st44_fsm_43 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st44_fsm_43 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_319) begin
    if (ap_sig_bdd_319) begin
        ap_sig_cseq_ST_st45_fsm_44 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st45_fsm_44 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_277) begin
    if (ap_sig_bdd_277) begin
        ap_sig_cseq_ST_st49_fsm_48 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st49_fsm_48 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_171) begin
    if (ap_sig_bdd_171) begin
        ap_sig_cseq_ST_st4_fsm_3 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st4_fsm_3 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_196) begin
    if (ap_sig_bdd_196) begin
        ap_sig_cseq_ST_st8_fsm_7 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st8_fsm_7 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_331) begin
    if (ap_sig_bdd_331) begin
        ap_sig_cseq_ST_st9_fsm_8 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st9_fsm_8 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st45_fsm_44 or ap_sig_cseq_ST_st9_fsm_8 or ap_sig_cseq_ST_st20_fsm_19) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st20_fsm_19)) begin
        grp_fu_203_opcode = ap_const_lv2_1;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st45_fsm_44) | (ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8))) begin
        grp_fu_203_opcode = ap_const_lv2_0;
    end else begin
        grp_fu_203_opcode = 'bx;
    end
end

always @ (reg_238 or storemerge_reg_135 or x_out_reg_180 or ap_sig_cseq_ST_st45_fsm_44 or ap_sig_cseq_ST_st9_fsm_8 or ap_sig_cseq_ST_st20_fsm_19) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st45_fsm_44)) begin
        grp_fu_203_p0 = x_out_reg_180;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st20_fsm_19)) begin
        grp_fu_203_p0 = reg_238;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8)) begin
        grp_fu_203_p0 = storemerge_reg_135;
    end else begin
        grp_fu_203_p0 = 'bx;
    end
end

always @ (v_reg_486 or tmp_47_i_reg_537 or max_3_i_reg_157 or ap_sig_cseq_ST_st45_fsm_44 or ap_sig_cseq_ST_st9_fsm_8 or ap_sig_cseq_ST_st20_fsm_19) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st45_fsm_44)) begin
        grp_fu_203_p1 = tmp_47_i_reg_537;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st20_fsm_19)) begin
        grp_fu_203_p1 = max_3_i_reg_157;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8)) begin
        grp_fu_203_p1 = v_reg_486;
    end else begin
        grp_fu_203_p1 = 'bx;
    end
end

always @ (l1_addr_reg_443 or ap_sig_cseq_ST_st4_fsm_3 or ap_sig_cseq_ST_st14_fsm_13 or ap_sig_cseq_ST_st18_fsm_17 or l1_addr_2_reg_522 or tmp_36_i_fu_317_p1 or tmp_44_i_fu_425_p1 or ap_sig_cseq_ST_st45_fsm_44) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st45_fsm_44)) begin
        l1_address0 = l1_addr_2_reg_522;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3)) begin
        l1_address0 = l1_addr_reg_443;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17)) begin
        l1_address0 = tmp_44_i_fu_425_p1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st14_fsm_13)) begin
        l1_address0 = tmp_36_i_fu_317_p1;
    end else begin
        l1_address0 = 'bx;
    end
end

always @ (ap_sig_cseq_ST_st4_fsm_3 or ap_sig_cseq_ST_st14_fsm_13 or ap_sig_cseq_ST_st18_fsm_17 or ap_sig_cseq_ST_st45_fsm_44) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3) | (ap_const_logic_1 == ap_sig_cseq_ST_st14_fsm_13) | (ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17) | (ap_const_logic_1 == ap_sig_cseq_ST_st45_fsm_44))) begin
        l1_ce0 = ap_const_logic_1;
    end else begin
        l1_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st4_fsm_3 or tmp_47_i_reg_537 or storemerge_reg_135 or ap_sig_cseq_ST_st45_fsm_44) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st45_fsm_44)) begin
        l1_d0 = tmp_47_i_reg_537;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3)) begin
        l1_d0 = storemerge_reg_135;
    end else begin
        l1_d0 = 'bx;
    end
end

always @ (ap_sig_cseq_ST_st4_fsm_3 or ap_sig_cseq_ST_st45_fsm_44) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3) | (ap_const_logic_1 == ap_sig_cseq_ST_st45_fsm_44))) begin
        l1_we0 = ap_const_logic_1;
    end else begin
        l1_we0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st2_fsm_1) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1)) begin
        lb1_ce0 = ap_const_logic_1;
    end else begin
        lb1_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st4_fsm_3) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3)) begin
        lin_ce0 = ap_const_logic_1;
    end else begin
        lin_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st4_fsm_3) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3)) begin
        lw1_ce0 = ap_const_logic_1;
    end else begin
        lw1_ce0 = ap_const_logic_0;
    end
end
always @ (ap_CS_fsm or exitcond5_i_fu_244_p2 or exitcond4_i_fu_274_p2 or exitcond3_i_fu_305_p2 or exitcond2_i_fu_413_p2 or ap_sig_bdd_287) begin
    case (ap_CS_fsm)
        ap_ST_st1_fsm_0 : 
        begin
            if (~ap_sig_bdd_287) begin
                ap_NS_fsm = ap_ST_st2_fsm_1;
            end else begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end
        end
        ap_ST_st2_fsm_1 : 
        begin
            if (~(exitcond5_i_fu_244_p2 == ap_const_lv1_0)) begin
                ap_NS_fsm = ap_ST_st14_fsm_13;
            end else begin
                ap_NS_fsm = ap_ST_st3_fsm_2;
            end
        end
        ap_ST_st3_fsm_2 : 
        begin
            ap_NS_fsm = ap_ST_st4_fsm_3;
        end
        ap_ST_st4_fsm_3 : 
        begin
            if (~(ap_const_lv1_0 == exitcond4_i_fu_274_p2)) begin
                ap_NS_fsm = ap_ST_st2_fsm_1;
            end else begin
                ap_NS_fsm = ap_ST_st5_fsm_4;
            end
        end
        ap_ST_st5_fsm_4 : 
        begin
            ap_NS_fsm = ap_ST_st6_fsm_5;
        end
        ap_ST_st6_fsm_5 : 
        begin
            ap_NS_fsm = ap_ST_st7_fsm_6;
        end
        ap_ST_st7_fsm_6 : 
        begin
            ap_NS_fsm = ap_ST_st8_fsm_7;
        end
        ap_ST_st8_fsm_7 : 
        begin
            ap_NS_fsm = ap_ST_st9_fsm_8;
        end
        ap_ST_st9_fsm_8 : 
        begin
            ap_NS_fsm = ap_ST_st10_fsm_9;
        end
        ap_ST_st10_fsm_9 : 
        begin
            ap_NS_fsm = ap_ST_st11_fsm_10;
        end
        ap_ST_st11_fsm_10 : 
        begin
            ap_NS_fsm = ap_ST_st12_fsm_11;
        end
        ap_ST_st12_fsm_11 : 
        begin
            ap_NS_fsm = ap_ST_st13_fsm_12;
        end
        ap_ST_st13_fsm_12 : 
        begin
            ap_NS_fsm = ap_ST_st4_fsm_3;
        end
        ap_ST_st14_fsm_13 : 
        begin
            if (~(ap_const_lv1_0 == exitcond3_i_fu_305_p2)) begin
                ap_NS_fsm = ap_ST_st18_fsm_17;
            end else begin
                ap_NS_fsm = ap_ST_st15_fsm_14;
            end
        end
        ap_ST_st15_fsm_14 : 
        begin
            ap_NS_fsm = ap_ST_st16_fsm_15;
        end
        ap_ST_st16_fsm_15 : 
        begin
            ap_NS_fsm = ap_ST_st17_fsm_16;
        end
        ap_ST_st17_fsm_16 : 
        begin
            ap_NS_fsm = ap_ST_st14_fsm_13;
        end
        ap_ST_st18_fsm_17 : 
        begin
            if (~(ap_const_lv1_0 == exitcond2_i_fu_413_p2)) begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end else begin
                ap_NS_fsm = ap_ST_st19_fsm_18;
            end
        end
        ap_ST_st19_fsm_18 : 
        begin
            ap_NS_fsm = ap_ST_st20_fsm_19;
        end
        ap_ST_st20_fsm_19 : 
        begin
            ap_NS_fsm = ap_ST_st21_fsm_20;
        end
        ap_ST_st21_fsm_20 : 
        begin
            ap_NS_fsm = ap_ST_st22_fsm_21;
        end
        ap_ST_st22_fsm_21 : 
        begin
            ap_NS_fsm = ap_ST_st23_fsm_22;
        end
        ap_ST_st23_fsm_22 : 
        begin
            ap_NS_fsm = ap_ST_st24_fsm_23;
        end
        ap_ST_st24_fsm_23 : 
        begin
            ap_NS_fsm = ap_ST_st25_fsm_24;
        end
        ap_ST_st25_fsm_24 : 
        begin
            ap_NS_fsm = ap_ST_st26_fsm_25;
        end
        ap_ST_st26_fsm_25 : 
        begin
            ap_NS_fsm = ap_ST_st27_fsm_26;
        end
        ap_ST_st27_fsm_26 : 
        begin
            ap_NS_fsm = ap_ST_st28_fsm_27;
        end
        ap_ST_st28_fsm_27 : 
        begin
            ap_NS_fsm = ap_ST_st29_fsm_28;
        end
        ap_ST_st29_fsm_28 : 
        begin
            ap_NS_fsm = ap_ST_st30_fsm_29;
        end
        ap_ST_st30_fsm_29 : 
        begin
            ap_NS_fsm = ap_ST_st31_fsm_30;
        end
        ap_ST_st31_fsm_30 : 
        begin
            ap_NS_fsm = ap_ST_st32_fsm_31;
        end
        ap_ST_st32_fsm_31 : 
        begin
            ap_NS_fsm = ap_ST_st33_fsm_32;
        end
        ap_ST_st33_fsm_32 : 
        begin
            ap_NS_fsm = ap_ST_st34_fsm_33;
        end
        ap_ST_st34_fsm_33 : 
        begin
            ap_NS_fsm = ap_ST_st35_fsm_34;
        end
        ap_ST_st35_fsm_34 : 
        begin
            ap_NS_fsm = ap_ST_st36_fsm_35;
        end
        ap_ST_st36_fsm_35 : 
        begin
            ap_NS_fsm = ap_ST_st37_fsm_36;
        end
        ap_ST_st37_fsm_36 : 
        begin
            ap_NS_fsm = ap_ST_st38_fsm_37;
        end
        ap_ST_st38_fsm_37 : 
        begin
            ap_NS_fsm = ap_ST_st39_fsm_38;
        end
        ap_ST_st39_fsm_38 : 
        begin
            ap_NS_fsm = ap_ST_st40_fsm_39;
        end
        ap_ST_st40_fsm_39 : 
        begin
            ap_NS_fsm = ap_ST_st41_fsm_40;
        end
        ap_ST_st41_fsm_40 : 
        begin
            ap_NS_fsm = ap_ST_st42_fsm_41;
        end
        ap_ST_st42_fsm_41 : 
        begin
            ap_NS_fsm = ap_ST_st43_fsm_42;
        end
        ap_ST_st43_fsm_42 : 
        begin
            ap_NS_fsm = ap_ST_st44_fsm_43;
        end
        ap_ST_st44_fsm_43 : 
        begin
            ap_NS_fsm = ap_ST_st45_fsm_44;
        end
        ap_ST_st45_fsm_44 : 
        begin
            ap_NS_fsm = ap_ST_st46_fsm_45;
        end
        ap_ST_st46_fsm_45 : 
        begin
            ap_NS_fsm = ap_ST_st47_fsm_46;
        end
        ap_ST_st47_fsm_46 : 
        begin
            ap_NS_fsm = ap_ST_st48_fsm_47;
        end
        ap_ST_st48_fsm_47 : 
        begin
            ap_NS_fsm = ap_ST_st49_fsm_48;
        end
        ap_ST_st49_fsm_48 : 
        begin
            ap_NS_fsm = ap_ST_st18_fsm_17;
        end
        default : 
        begin
            ap_NS_fsm = 'bx;
        end
    endcase
end


assign ap_return = x_out_reg_180;


always @ (ap_CS_fsm) begin
    ap_sig_bdd_111 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_C]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_118 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_17]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_127 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_E]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_134 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_12]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_144 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_1]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_161 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_2]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_171 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_3]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_196 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_7]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_205 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_D]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_219 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_F]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_228 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_10]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_237 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_11]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_251 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_18]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_260 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_2A]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_269 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_2B]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_277 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_30]);
end


always @ (ap_start or ap_done_reg) begin
    ap_sig_bdd_287 = ((ap_start == ap_const_logic_0) | (ap_done_reg == ap_const_logic_1));
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_319 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_2C]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_331 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_8]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_338 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_13]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_68 = (ap_CS_fsm[ap_const_lv32_0] == ap_const_lv1_1);
end

assign exitcond2_i_fu_413_p2 = (i_i_reg_192 == ap_const_lv4_A? 1'b1: 1'b0);

assign exitcond3_i_fu_305_p2 = (i_9_i_reg_169 == ap_const_lv4_A? 1'b1: 1'b0);

assign exitcond4_i_fu_274_p2 = (j_7_i_reg_146 == ap_const_lv7_40? 1'b1: 1'b0);

assign exitcond5_i_fu_244_p2 = (i_8_i_reg_123 == ap_const_lv4_A? 1'b1: 1'b0);

assign grp_fu_203_ce = ap_const_logic_1;

assign grp_fu_210_ce = ap_const_logic_1;

assign grp_fu_227_ce = ap_const_logic_1;

assign i_1_fu_250_p2 = (i_8_i_reg_123 + ap_const_lv4_1);

assign i_2_fu_419_p2 = (i_i_reg_192 + ap_const_lv4_1);

assign i_fu_311_p2 = (i_9_i_reg_169 + ap_const_lv4_1);

assign j_fu_280_p2 = (j_7_i_reg_146 + ap_const_lv7_1);

assign lb1_address0 = tmp_28_i_fu_256_p1;

assign lin_address0 = tmp_38_i_fu_286_p1;

assign lw1_address0 = tmp_34_cast_fu_300_p1;

assign max_2_fu_405_p3 = ((tmp_13_fu_400_p2[0:0] === 1'b1) ? reg_238 : max_3_i_reg_157);

assign max_3_i_to_int_fu_340_p1 = max_3_i_reg_157;

assign max_to_int_fu_322_p1 = reg_238;

assign notlhs1_fu_376_p2 = (tmp_7_fu_344_p4 != ap_const_lv8_FF? 1'b1: 1'b0);

assign notlhs_fu_358_p2 = (tmp_fu_326_p4 != ap_const_lv8_FF? 1'b1: 1'b0);

assign notrhs2_fu_382_p2 = (tmp_23_fu_354_p1 == ap_const_lv23_0? 1'b1: 1'b0);

assign notrhs_fu_364_p2 = (tmp_22_fu_336_p1 == ap_const_lv23_0? 1'b1: 1'b0);

assign tmp_10_fu_388_p2 = (notrhs2_fu_382_p2 | notlhs1_fu_376_p2);

assign tmp_11_fu_394_p2 = (tmp_9_fu_370_p2 & tmp_10_fu_388_p2);

assign tmp_12_fu_222_opcode = ap_const_lv5_2;

assign tmp_13_fu_400_p2 = (tmp_11_fu_394_p2 & tmp_12_reg_504);

assign tmp_20_fu_295_p2 = (tmp_33_cast_reg_448 + tmp_38_i_cast_fu_291_p1);

assign tmp_22_fu_336_p1 = max_to_int_fu_322_p1[22:0];

assign tmp_23_fu_354_p1 = max_3_i_to_int_fu_340_p1[22:0];

assign tmp_28_i_fu_256_p1 = i_8_i_reg_123;

assign tmp_33_cast_fu_270_p1 = tmp_s_fu_262_p3;

assign tmp_34_cast_fu_300_p1 = tmp_20_fu_295_p2;

assign tmp_36_i_fu_317_p1 = i_9_i_reg_169;

assign tmp_38_i_cast_fu_291_p1 = j_7_i_reg_146;

assign tmp_38_i_fu_286_p1 = j_7_i_reg_146;

assign tmp_44_i_fu_425_p1 = i_i_reg_192;

assign tmp_7_fu_344_p4 = {{max_3_i_to_int_fu_340_p1[ap_const_lv32_1E : ap_const_lv32_17]}};

assign tmp_9_fu_370_p2 = (notrhs_fu_364_p2 | notlhs_fu_358_p2);

assign tmp_fu_326_p4 = {{max_to_int_fu_322_p1[ap_const_lv32_1E : ap_const_lv32_17]}};

assign tmp_s_fu_262_p3 = {{i_8_i_reg_123}, {ap_const_lv6_0}};
always @ (posedge ap_clk) begin
    tmp_33_cast_reg_448[5:0] <= 6'b000000;
    tmp_33_cast_reg_448[10] <= 1'b0;
end



endmodule //cnn_Loop_Lin1_proc

